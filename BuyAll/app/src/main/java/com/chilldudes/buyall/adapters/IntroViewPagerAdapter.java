package com.chilldudes.buyall.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.chilldudes.buyall.R;
import com.chilldudes.buyall.items.ScreenItems;

import java.util.List;


public class IntroViewPagerAdapter extends PagerAdapter {

    Context mContext;
    List<ScreenItems> mListScreen;

    public IntroViewPagerAdapter(Context mContext, List<ScreenItems> mListScreen) {
        this.mContext = mContext;
        this.mListScreen = mListScreen;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutScreen = inflater.inflate((R.layout.layout_screen_intro), null);
        ImageView imgSlide = layoutScreen.findViewById(R.id.slideImgV_intro);
        TextView title = layoutScreen.findViewById(R.id.headingTV_intro);
        TextView description = layoutScreen.findViewById(R.id.infoTV_intro);


        title.setText(mListScreen.get(position).getTitle());
        description.setText(mListScreen.get(position).getDescription());
        imgSlide.setImageResource(mListScreen.get(position).getScreenImg());

        container.addView(layoutScreen);

        return layoutScreen;
    }

    public Context getmContext() {
        return mContext;
    }

    public List<ScreenItems> getmListScreen() {
        return mListScreen;
    }

    @Override
    public int getCount() {
        return mListScreen.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
